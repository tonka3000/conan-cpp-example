# Conan features on GitLab.com
This repo demonstrate [conan](https://conan.io) features on GitLab.com including

* [Windows Shared Runner](https://gitlab.com/gitlab-org/ci-cd/shared-runners/images/gcp/windows-containers/blob/master/cookbooks/preinstalled-software/README.md)
* [GitLab Conan Registry](https://docs.gitlab.com/ee/user/packages/conan_repository)

and conan itself.

## Environments
This repo support multiple environments
* [GitPod](https://www.gitpod.io)<br>
  So just fork it and try it out thanks to [GitLab's GitPod integration](https://docs.gitlab.com/ee/integration/gitpod.html)
* [VS Code Remote Containers](https://code.visualstudio.com/docs/remote/containers)<br>
  Just open VS Code and open the repo in a devcontainer.
* Local development

## Build
You can build this repo via cmake. You have 2 options
* build via commandline<br>
  You can type `cmake -H. -Bbuild && cmake --build build` to build the project like any other cmake project. 
  By default this cmake script will build in `Release` mode. If you want to debug build it in `Debug` mode via 
  `cmake -H. -Bbuild && cmake --build build -DCMAKE_BUILD_TYPE=Debug`
* You can use the cmake integration e.g. like VSCode and use the builtin commands for that

This repo use [cmake conan](https://github.com/conan-io/cmake-conan) which is a cmake wrapper for conan. The benefit is that you don't need to
now know the conan commands, the wrapper will extract the data from cmake and call conan command internally based on that data. The wrapper itself 
will be downloaded by the cmake script itself. You don't have to use this wrapper, but it makes it easier to use conan with cmake.

## CI
The repo also contains a GitLab-CI config for Linux (shared runner from gitlab.com) and Windows (Windows shared runner from gitlab.com)

## conan remotes
This project use 2 conan remotes
* conan-center
* bincrafters

TODO: Add GitLab conan registry from GitLab.com when issue #1 is fixed

## Targets
### heyconsole
`heyconsole` is just an simple hello world executable without any dependency.

### heyboost
`heyboost` is a executable with boost (from conan-center)

### heyqt
`heyqt` is a executable with qt (from bincrafters). This use Qt's widgets system, so this example can only run in a real desktop environment.

:warning: This does not work in any environment right now, so the code is commented out. If you want to try it just remove the comments from `src/CMakeLists.txt` 
and `CMake3rdparty.cmake` :warning:
